<?php

/**
 * Extension Manager/Repository config file for ext "ubbs_anmeldeformular".
 */
$EM_CONF[$_EXTKEY] = [
    'title' => 'ubbs anmeldeformular',
    'description' => 'Anmeldeformular der UB Braunschweig als Typo3-Extension',
    'category' => 'plugin',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-9.5.99',
            'rte_ckeditor' => '8.7.0-9.5.99'
        ],
        'conflicts' => [
        ],
    ],
    'autoload' => [
        'psr-4' => [
            'UniversitaetsbibliothekBraunschweig\\UbbsAnmeldeformular\\' => 'Classes'
        ],
    ],
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1,
    'author' => 'Henning Peters',
    'author_email' => 'henning.peters@tu-braunschweig.de',
    'author_company' => 'Universitätsbibliothek Braunschweig',
    'version' => '1.0.0',
];
