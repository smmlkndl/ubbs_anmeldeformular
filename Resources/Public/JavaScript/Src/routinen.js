$(document).ready(function(){
	checkInvalid();																					//Symbol bei erforderlichen Feldern
	//$("#anmeldeformular input, select").removeAttr("required");
	//Dynamischer Aufruf der Routinen bei Änderungen im Anmeldeformular

	$(document).on("change", "#anmeldeformular *", function(){
		checkAlter();																					//Anmeldung erst mit 16
		checkMailbox();																					//schaut nach, ob E-Mail-Kommunikation gewünscht ist;
		checkTU();																						//schaut nach, ob E-Mails @tu-braunschweig.de enthalten müssen
		checkMatch();																					//schaut nach, ob E-Mails übereinstimmen
		getAdresstype();																				//ermittelt den Adresstypem, steuert dadurch die Anzeige der Adressüberschriften und ändert zwei Felder in Required und zurück'/
		checkInvalid();																					//Symbol bei erforderlichen Feldern
    });

	//FUNKTIONEN

	function checkInvalid(){
		$("#anmeldeformular *:invalid").prev("label").addClass("mustFill");
		$("#anmeldeformular *:valid").prev("label").removeClass("mustFill");
	}


	function checkAlter(){
		if($("#birthday_date").val().length > 0){
			var mindestAlter = 5844;
			var today = new Date();
			var heute = today.toISOString().substring(0, 10);
			var eingabeJahr = $("#birthday_date").val();
			var alterinTagen = (Date.parse(heute)-Date.parse(eingabeJahr)) /86400000;
			console.log(alterinTagen);
			if(alterinTagen >= mindestAlter){
				$("#birthday_date")[0].setCustomValidity('');
			} else {

				$("#birthday_date")[0].setCustomValidity('Anmeldung erst ab 16 Jahren möglich / Registration only from the age of 16');
			}	
		}
	}

	function checkMailbox(){
	
		if($("#email_checkbox").is(":checked")){
			$("#email").prop("required", true);
			$("#email_confirm").prop("required", true);
			$("#email_checkbox").val("t");
		} 
		else if(!$("#email_checkbox").is(":checked") && $("#email").val() == ""){
				$("#email").removeAttr("required");
				$("#email_confirm").removeAttr("required");
				$("#email_checkbox").val("f");
		} else {
			$("#email_checkbox").val("f");
		}
	}


	function getAdresstype(){

		var ut = $("#usertype").val();
		if(ut == 1 || ut == 2 || ut == 3){																					//Wenn Usertype = 1,2,3 ist dann ist Matrikelnummer erforderlich
            $("#student_id").prop("required", true);
		} 
		else{
			$("#student_id").removeAttr("required");
		}

        switch(ut){
            case '1': case '2': case '3': case '14' : case '15':
                $("#ersteAdresse").text("Semesteranschrift / Semester Adress");
                $("#adresstype_1").val(1);
                $("#zweiteAdresse").text("Heimatanschrift / Home Adress");
				$("#adresstype_2").val(7);
				$("#carry_over_1").removeAttr("required");
                break;
            case '4':
                    $("#ersteAdresse").text("Institutsanschrift / Institute Address");
                    $("#adresstype_1").val(2);
                    $("#zweiteAdresse").text("Privatanschrift / Private Adress");
					$("#adresstype_2").val(4);
					$("#carry_over_1").prop("required", true);
                    break;
            case '5':
                    $("#ersteAdresse").text("Institutsanschrift / Institute Address");
                    $("#adresstype_1").val(2);
                    $("#zweiteAdresse").text("Privatanschrift / Private Adress");
					$("#adresstype_2").val(4);
					$("#carry_over_1").removeAttr("required");
                    break;  
            case '6':
                    $("#ersteAdresse").text("Dienstanschrift / Adress at Work");
                    $("#adresstype_1").val(3);
                    $("#zweiteAdresse").text("Privatanschrift / Private Adress");
					$("#adresstype_2").val(4);
					$("#carry_over_1").removeAttr("required");
                    break;                 
            default:
                    $("#ersteAdresse").text("Erster Wohnsitz / First Address");
                    $("#adresstype_1").val(5);
                    $("#zweiteAdresse").text("Weiterer Wohnsitz / Second Adress");
					$("#adresstype_2").val(6);
					$("#carry_over_1").removeAttr("required");
                    break;                                                
        }

	}
	function checkTU(){
		
		if($("#usertype").val() == 1 && $("#email_checkbox").val() == "t" || $("#email").val() != ""){

			var mailinhalt = String($("#email").val());
			if(mailinhalt.match("@tu-braunschweig.de")){
				$("#email")[0].setCustomValidity('');
				$("#email_confirm")[0].setCustomValidity('');
			}
			else {
				$("#email")[0].setCustomValidity('Bitte TU-Mailadresse angeben / Please enter your TU-Mailadress');
				$("#email_confirm")[0].setCustomValidity('Bitte TU-Mailadresse angeben / Please enter your TU-Mailadress');
			}
		}
		else {
			$("#email")[0].setCustomValidity('');
			$("#email_confirm")[0].setCustomValidity('');
		}
	}

	function checkMatch() {
		if ($("#email").val() != $("#email_confirm").val()) {
				$("#email")[0].setCustomValidity('Emails stimmen nicht überein / Mails don`t match');
				$("#email_confirm")[0].setCustomValidity('Emails stimmen nicht überein / Mails don`t match');
			}
			else if($("#email").val() == $("#email_confirm").val() && $("#usertype").val() !=1) {
		// input is valid -- reset the error message
				$("#email")[0].setCustomValidity('');
				$("#email_confirm")[0].setCustomValidity('');
			}
			else{
				checkTU();
			}
		 
	}

	

});
