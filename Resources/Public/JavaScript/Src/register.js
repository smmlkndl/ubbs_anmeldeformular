$("#anmeldeformular").submit(function(event){
    event.preventDefault();
	/*
		Folgende Felder werden in die Personen-Tabelle benötigt
		id 				=> wird automatisch gesetzt
		last_name		=> Formularfeld
		first_name		=> Formularfeld
		title			=> Formularfeld
		sex				=> Formularfeld
		birthday		=> Formularfeld
		usertype_id		=> muss aus Status und Angabe von Mail ermittelt werden -> getfinalusertype()
		email_checkbox	=> Formularfeld
		email			=> Formularfeld
		email_confirm	=> Formularfeld
		student_id		=> Formularfeld
		status			=> ist new
		entry_date		=> heutiges_datum

		Folgende Felder werden in der Adresse-Tabelle benötigt
			Anm.: Da für jede Person zwei Datenreihen in der Adressen-Tabelle geschrieben werden, werden auch zwei Adress-Objekte übergeben
		id 				=> wird automatisch gesetzt
		person_id		=> stammt aus Personendatenbank und wird im PHP-Skript eingefügt
		carry_over		=> Formularfeld
		street			=> Formularfeld
		house			=> Formularfeld
		room			=> Formularfeld
		zip				=> Formularfeld
		town			=> Formularfeld
		phone			=> Formularfeld
		mobile_phone	=> Formularfeld
		is_primary		=> wenn Adresse 2 gesetzt, dann ist diese is_primary = true
	
	*/	



	//Ermittelung von Werten, die nicht direkt aus dem Anmeldeformular stammen: usertype_id und entry_date
	var finalusertype = 0;
	var today = new Date();
	var entry_date = today.toISOString().substring(0, 10);

	getfinalusertype();
	//Usertype wird anhand von ausgewähltem Status und Angabe Mail ja oder nein ermittelt
	function getfinalusertype(){
		var usertype = $("#usertype").val();
		var mailck = false;
		if($("#email").val().length > 0){ mailck = true;}
			
		switch(usertype){

			case '1':
				if(mailck == false){ finalusertype = 1} else {finalusertype = 21 }
				break;
			case '2':
				if(mailck == false){ finalusertype = 2} else {finalusertype = 22 }
				break;
			case '3':
				if(mailck == false){ finalusertype = 3} else {finalusertype = 23 }
				break;
			case '4':
				if(mailck == false){ finalusertype = 4} else {finalusertype = 24 }
				break;
			case '5':
				if(mailck == false){ finalusertype = 5} else {finalusertype = 25 }
				break;				
			case '6':
				if(mailck == false){ finalusertype = 6} else {finalusertype = 26 }
				break;
			case '7':
				finalusertype = 7;
				break;
			case '9':
				if(mailck == false){ finalusertype = 9} else {finalusertype = 29 }
				break;
			case '10':
				if(mailck == false){ finalusertype = 10} else {finalusertype = 30 }
				break;
			case '11':
				if(mailck == false){ finalusertype = 11} else {finalusertype = 31 }
				break;;
			case '12':
				if(mailck == false){ finalusertype = 12} else {finalusertype = 32 }
				break;
			case '13':
				if(mailck == false){ finalusertype = 13} else {finalusertype = 33 }
				break;
			case '14':
				if(mailck == false){ finalusertype = 14} else {finalusertype = 34 }
				break;
			case '15':
				if(mailck == false){ finalusertype = 15} else {finalusertype = 35 }
				break;
			case '36':
				if(mailck == false){ finalusertype = 36} else {finalusertype = 56 }
				break;
		}
		
	}

	//Anmeldeobjekt person wird gebildet und als JSON-String umgewandelt per AJAX-Post-request an PHP-SKRIPT auf UB-Server gesendet
	

	var person = new Object;
		person.last_name = $("#last_name").val();
		person.first_name = $("#first_name").val();
		person.title = $("#title").val();
		person.sex = $("#sex").val();
		person.birthday = $("#birthday_date").val();
		person.usertype_id = finalusertype
		if($("#student_id").val().length >0){person.student_id = $("#student_id").val();}
		person.email_checkbox = $("#email_checkbox").val();
		if($("#email").val().length > 0){ person.email = $("#email").val();	}
		if($("#email_confirm").val().length > 0){person.email_confirm = $("#email").val();}
		person.status = "new";
		person.entry_date = entry_date;

	var adresse_1 = new Object;
		adresse_1.carry_over = $("#carry_over_1").val();
		adresse_1.street = $("#street_1").val();
		adresse_1.house = $("#house_1").val();
		if($("#room_1").val().length > 0){
			adresse_1.room = $("#room_1").val();
		}	
		adresse_1.zip = $("#zip_1").val();
		adresse_1.town = $("#town_1").val();
		adresse_1.phone = $("#phone_1").val();
		adresse_1.mobile_phone = $("#mobile_1").val();
		if($("#street_2").val().length > 0){adresse_1.is_primary = "false";}
		else{ adresse_1.is_primary = "true";}

	var adresse_2 = new Object;
		adresse_2.carry_over = $("#carry_over_2").val();
		adresse_2.street = $("#street_2").val();
		adresse_2.house = $("#house_2").val();
		adresse_2.room = $("#room_2").val();
		adresse_2.zip = $("#zip_2").val();
		adresse_2.town = $("#town_2").val();
		adresse_2.phone = $("#phone_1").val();
		adresse_2.mobile_phone = $("#mobile_1").val();
		if($("#street_2").val().length > 0){adresse_2.is_primary = "true";}
		else{ adresse_2.is_primary = "false";}

	//Umwandlung der Drei Objekte in Strings
	personjson = JSON.stringify(person);
	adresse_1json = JSON.stringify(adresse_1);
	adresse_2json = JSON.stringify(adresse_2);
	$.ajax({
        url: "https://ub.tu-braunschweig.de/scripts/php/registert3.php",				//Speicherort vom PHP-Skript um in die Datenbank zu schreiben
        type: "POST",
        data: ({person : personjson, adresse_1: adresse_1json, adresse_2: adresse_2json}),
        async: false,
        cache: false,
        error: onError,
            success: function(data){
            $("#anmeldeformular").html(data);        
            }
	});
	function onError(xhr, txtStatus, errThrown) {
		console.log(
			"XMLHttpRequest: ", xhr,
			" Status:", txtStatus,
			" Error:",  errThrown
		);
	}
});