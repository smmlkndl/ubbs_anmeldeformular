$(document).ready(function(){
    console.log("READY");
    $("#anmeldeformular input").removeAttr("required");

    // setzt die email-felder auf required, wenn die Email-Checkbox angeklickt wurde, sowie das value von email_checkbox von f auf t
    $(document).on("change", "#email_checkbox", function(){
       if($("#email_checkbox").is(":checked")){
           $("#email").prop("required", true);

           $("#email_confirm").prop("required", true);
           $(this).val("t");
       } 
       else if(!$("#email_checkbox").is(":checked") && $("#email").val() == ""){
            $("#email").removeAttr("required");
            $("#email_confirm").removeAttr("required");
            $(this).val("f");
       } else {
           $(this).val("f");
       }
    });
    //E-Mail-Adressen: Überprüfung, ob beide Felder gleiche Inhalte haben

    $(document).on("keyup", ".emails", function(){
        if($(".emails").length != 0 && $("#usertype").val() != 1){
            function checkMatch() {
                if ($("#email").val() != $("#email_confirm").val()) {
                    $("#email")[0].setCustomValidity('Emails stimmen nicht überein / Mails don`t match');
                    $("#email_confirm")[0].setCustomValidity('Emails stimmen nicht überein / Mails don`t match');
                }
                else {
                // input is valid -- reset the error message
                    $("#email")[0].setCustomValidity('');
                    $("#email_confirm")[0].setCustomValidity('');
                }
            }
           checkMatch();
        }

    });
    //E-Mail-Adressen: Überprüfung, ob TU-Studierende @tu-braunschweig.de angegeben haben
    $(document).on("change", "#usertype", function(){
        if($("#usertype").val() == 1){
            $("#email_checkbox").prop("checked", true);
            $("#email_checkbox").val("t");
            function checkTU(){
                if($("#email").val().indexOf("@tu-braunschweig.de")){
                   $("#email")[0].setCustomValidity('Sie müssen Ihre TU-Mailadresse angeben / Please insert your TU-Mailadress');
                   $("#email_confirm")[0].setCustomValidity('Sie müssen Ihre TU-Mailadresse angeben / Please insert your TU-Mailadress');
                } 
                else{
                    $("#email")[0].setCustomValidity(''); 
                    $("#email_confirm")[0].setCustomValidity('');
                }
            }
            checkTU();    
        } else {
            $("#email_checkbox").prop("checked", false);
            $("#email_checkbox").val("f");
            $("#email")[0].setCustomValidity(''); 
            $("#email_confirm")[0].setCustomValidity('');          
        }
    });   

    // Macht Matrikelnummer zum erforderlichen Feld bei TU,HBK und Ostfalia
    $(document).on("change", "#usertype", function(){
        if($("#usertype").val() <= 3){
            $("#student_id").prop("required", true);
        } else {
            $("#student_id").removeAttr("required");
        }
    });   

    //Ändert die Überschriften der Anschriften

    $(document).on("change", "#usertype", function(){
        var ut = $("#usertype").val();
        switch(ut){
            case '1': case '2': case '3': case '14' : case '15':
                $("#ersteAdresse").text("Semesteranschrift / Semester Adress");
                $("#adresstype_1").val(1);
                $("#zweiteAdresse").text("Heimatanschrift / Home Adress");
                $("#adresstype_2").val(7);
                break;
            case '4':
                    $("#ersteAdresse").text("Institutsanschrift / Institute Address");
                    $("#adresstype_1").val(2);
                    $("#zweiteAdresse").text("Privatanschrift / Private Adress");
                    $("#adresstype_2").val(4);
                    $("#carry_over_1").prop("required", true);
                    break;
            case '5':
                    $("#ersteAdresse").text("Institutsanschrift / Institute Address");
                    $("#adresstype_1").val(2);
                    $("#zweiteAdresse").text("Privatanschrift / Private Adress");
                    $("#adresstype_2").val(4);
                    break;  
            case '6':
                    $("#ersteAdresse").text("Dienstanschrift / Adress at Work");
                    $("#adresstype_1").val(3);
                    $("#zweiteAdresse").text("Privatanschrift / Private Adress");
                    $("#adresstype_2").val(4);
                    break;                 
            default:
                    $("#ersteAdresse").text("Erster Wohnsitz / First Address");
                    $("#adresstype_1").val(5);
                    $("#zweiteAdresse").text("Weiterer Wohnsitz / Second Adress");
                    $("#carry_over_1").prop("required", false);
                    $("#adresstype_2").val(6);
                    break;                                                
        }

    });

    




    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*
        if(ut == 1){
           
        }
    });
    /*
    $(document).on("change", "#usertype", function(){
        var ut = $("#usertype").val();
        if(ut == 1){
            $("#ersteAdresse").text("Semesteranschrift / Semester adress");
            $("#adresstype_1").val(1);
            $("#zweiteAdresse").text("Heimatanschrift / Home adress");
            $("#adresstype_2").val(7);
        }
    });
*/
});

/*
$("#mailwunsch").change(function(){
   if($('#mailwunsch').is(":checked")){
        $("#mailadresse").prop("required", true);
        $("#mailadressewdh").prop("required", true);

   } else {
        $("#mailadresse").prop("required", false);
        $("#mailadressewdh").prop("required", false);
   }
});

$(".anmeldermail").keyup(function(){
    if($("#mailadresse").val() === "" && $("#mailadressewdh").val() === ""){
        $("#mailadressewdh").prop("required", false);
        $("#mailmismatch").html("");
    } else {
        $("#mailadressewdh").prop("required", true);
        if(($("#mailadressewdh").val()) != $("#mailadresse").val()){
            $("#mailadressewdh").is(":invalid");
            $("#mailmismatch").html("Mailadressen stimmen nicht Ã¼berein");
        } else {
            $("#mailmismatch").html("");
        }
    }
});

$("#anmeldeformular").submit(function(){
    var baum = new Object;
    baum.likea = $("#titel").val();
    baum.titel = $("#name").val();
    baum.nach = $("#vorname").val();
    var obj = JSON.stringify(baum);
 
});

$("#sendOff").click(function(){
    var anob = new Object;

    anob.titel = $("#titel").val();
    anob.name = $("#name").val();
    anob.vorname = $("#vorname").val();
    var jsnob = JSON.stringify(anob);

	$.ajax({
        url: "https://ub.tu-braunschweig.de/tests/typoanob.php",
        type: "POST",
        //data: ({query: "INSERT into anob ('titel', 'name', 'vorname') VALUES ('"+anob.titel+"','"+anob.name+"','"+anob.vorname+"')"}),
        data: ({val: jsnob}),
        cache: false,
        error: onError,
            success: function(data){
            $("#hakki").html(data);        
            }
    });
});

function onError(xhr, txtStatus, errThrown) {
    console.log(
        "XMLHttpRequest: ", xhr,
        " Status:", txtStatus,
        " Error:",  errThrown
    );
}
*/