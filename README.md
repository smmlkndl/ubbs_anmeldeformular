Sitepackage for the project "ubbs anmeldeformular"
==============================================================

This extension is the registration form of the Braunschweig University Library. The fields are displayed via Show.html, the check routines via the file routinen.js in JavaScript/src and the sending of the registration data via register.js in the same folder.

Have fun