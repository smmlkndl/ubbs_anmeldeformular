<?php
(function () {
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        'UniversitaetsbibliothekBraunschweig.UbbsAnmeldeformular',
        'formulartemplate',
        'Anmeldeformular UB Braunschweig'
    );
})();