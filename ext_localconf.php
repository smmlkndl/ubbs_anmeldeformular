<?php
defined('TYPO3_MODE') || die();

/***************
 * Add default RTE configuration
 */
$GLOBALS['TYPO3_CONF_VARS']['RTE']['Presets']['ubbs_anmeldeformular'] = 'EXT:ubbs_anmeldeformular/Configuration/RTE/Default.yaml';

call_user_func(
    function () {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'UniversitaetsbibliothekBraunschweig.UbbsAnmeldeformular',
            'formulartemplate',
            [
                'Formular' => 'show, create, send',
            ]
        );
    });
