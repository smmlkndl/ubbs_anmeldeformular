<?php
    namespace UniversitaetsbibliothekBraunschweig\UbbsAnmeldeformular\Controller;
    use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;

    class FormularController extends ActionController
    {
        public function showAction()
        {
            $patterns = [
                'date' => '(?:19|20)[0-9]{2}-(?:(?:0[1-9]|1[0-2])-(?:0[1-9]|1[0-9]|2[0-9])|(?:(?!02)(?:0[1-9]|1[0-2])-(?:30))|(?:(?:0[13578]|1[02])-31))',
                'email' => '[a-zA-Z0-9.-_]{1,}@[a-zA-Z0-9.-]{1,}[.]{1}[a-zA-Z0-9]{2,}',
                'email_tu' => '[a-zA-Z0-9.-_]{1,}@tu-braunschweig.de'
            ];
            $this->view->assign('date', $patterns['date'])->assign('mail', $patterns["email"])->assign('mail_tu', $patterns["email_tu"]);
        }
       public function createAction()
        {
           $anmelder = $this->request->getArguments($anob);
           $this->view->assign("anob", $anmelder);
        }
        public function sendAction()
        {
           return "Angemeldet";
        }

    }
 